#!/usr/bin/env bash

ssh pi@192.168.0.57 "pkill -f demo-0.0.1-SNAPSHOT.jar"
echo "Killed stuff"
mvn clean install
rsync -z --progress ./target/demo-0.0.1-SNAPSHOT.jar pi@192.168.0.57:/home/pi/Projects/demo-0.0.1-SNAPSHOT.jar
rsync -z --progress manifest.yml pi@192.168.0.57:/home/pi/Projects/manifest.yml
ssh pi@192.168.0.57 'cd /home/pi/Projects && nohup java -agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n -jar /home/pi/Projects/demo-0.0.1-SNAPSHOT.jar >> /home/pi/Projects/logfile.log 2>&1 &'