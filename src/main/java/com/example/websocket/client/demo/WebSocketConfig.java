package com.example.websocket.client.demo;

import com.example.websocket.client.demo.DOM.DataPointVTQ;
import com.example.websocket.client.demo.DOM.ResponseVTC;
import com.example.websocket.client.demo.DOM.WebsocketConsumer;
import com.example.websocket.client.demo.DistanceGetter.DistanceController;
import com.ge.predix.solsvc.ext.util.JsonMapper;
import com.ge.predix.solsvc.restclient.config.DefaultOauthRestConfig;
import com.ge.predix.solsvc.restclient.config.IOauthRestConfig;
import com.ge.predix.solsvc.restclient.impl.RestClient;
import com.ge.predix.solsvc.restclient.impl.RestClientImpl;
import com.ge.predix.solsvc.timeseries.bootstrap.client.TimeseriesClient;
import com.ge.predix.solsvc.timeseries.bootstrap.config.DefaultTimeseriesConfig;
import com.ge.predix.solsvc.timeseries.bootstrap.config.ITimeseriesConfig;
import com.ge.predix.solsvc.websocket.client.WebSocketClient;
import com.ge.predix.solsvc.websocket.client.WebSocketClientImpl;
import com.ge.predix.solsvc.websocket.config.DefaultWebSocketConfigForTimeseries;
import com.ge.predix.solsvc.websocket.config.IWebSocketConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

@Configuration
@EnableWebSocket
@EnableScheduling
@PropertySource({"file:./manifest.yml"})
public class WebSocketConfig {

@Bean
public DataGenerator getPiDataGenerator(){
  return new DistanceController(getDataQueue(), getSendQueue(), getWsQueue());
}

@Bean
public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
  return new PropertySourcesPlaceholderConfigurer();
}

@Bean
public TimeSeriesConsumer getTimeSeries() {
  return new TimeSeriesConsumer(getSendQueue());
}
@Bean
public IOauthRestConfig defaultOauthRestConfig() {
  return new DefaultOauthRestConfig();
}

@Bean
public IWebSocketConfig defaultWebSocketConfig() {
  return new DefaultWebSocketConfigForTimeseries();
}

@Bean
public ITimeseriesConfig defaultTimeseriesConfig() {
  return new DefaultTimeseriesConfig();
}

@Bean
public JsonMapper jsonMapper() {
  return new JsonMapper();
}

@Bean
public RestClient restClient() {
  return new RestClientImpl();
}

@Bean
public WebSocketClient webSocketClient() {
  return new WebSocketClientImpl();
}

@Bean
public TimeseriesClient timeseriesClient() {
  return new WSTimeseriesWrapper();
}

@Bean
public InMemoryQueue<DataPointVTQ> getDataQueue(){
  return new InMemoryQueue<>();
}

@Bean
public InMemoryQueue<ResponseVTC> getSendQueue(){
  return new InMemoryQueue<>();
}

@Bean
public InMemoryQueue<ResponseVTC> getWsQueue() {return new InMemoryQueue<>();}

@Bean
public WebsocketConsumer getWsConsumer() { return new WebsocketConsumer(getWsQueue());}

}
