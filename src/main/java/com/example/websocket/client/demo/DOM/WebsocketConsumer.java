package com.example.websocket.client.demo.DOM;

import com.example.websocket.client.demo.InMemoryQueue;
import com.example.websocket.client.demo.WebsocketClientEndpoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kpashov on 09/06/17.
 */
public class WebsocketConsumer {

private InMemoryQueue<ResponseVTC> responseVTCs;
private WebsocketClientEndpoint clientEndPoint;
@Value("${machineControllerId}") private String machineControllerId;
@Value("${websocketServerUri}") private String websocketServerUri;
private static final org.slf4j.Logger log = LoggerFactory.getLogger(WebsocketConsumer.class);

public WebsocketConsumer(InMemoryQueue<ResponseVTC> responseVTCs){
  this.responseVTCs = responseVTCs;
}


@Scheduled(fixedDelay = 500)
public void sendData(){
  try {
    if(clientEndPoint != null){
      if (clientEndPoint.getUserSession() != null){
        if (clientEndPoint.getUserSession().isOpen()){
          sendViaWebsocket(clientEndPoint);
        }
      } else {
        this.openWebsocket();
        sendViaWebsocket(clientEndPoint);
      }
    }else {
      this.openWebsocket();
      sendViaWebsocket(clientEndPoint);
    }

  } catch (Exception e) {
    log.error("Error running sendData: "+e.getLocalizedMessage());
    e.printStackTrace();
  }
}

private void openWebsocket(){
  try {
    clientEndPoint = new WebsocketClientEndpoint(new URI(websocketServerUri + machineControllerId));
  } catch (Exception e){
    log.error("Error connecting to Websocket " + e.getLocalizedMessage());
    e.printStackTrace();
  }
}

private void sendViaWebsocket(WebsocketClientEndpoint clientEndPoint) {
  try {

    ResponseVTC[] dvt = new ResponseVTC[8];

    Iterator<ResponseVTC> iterator = responseVTCs.iterator();

    while (iterator.hasNext()) {
      ResponseVTC r = iterator.next();
      if (dvt[r.getFeeder()]!=null){
        if(dvt[r.getFeeder()].getTimestamp() < r.getTimestamp()){
          dvt[r.getFeeder()] = r;
        }
        iterator.remove();
      } else {
        dvt[r.getFeeder()] = r;
      }
    }

    List<JSONData> list = generateDataMap(dvt);
    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    mapper.writeValue(writer, list);
    clientEndPoint.sendMessage(writer.toString());

  } catch (Exception e){
    log.error("Error in sendViaWebsocket: " + e.getLocalizedMessage());
    e.printStackTrace();
  }
}

List<JSONData> generateDataMap(ResponseVTC[] rvtc)
{
  List<JSONData> list = new ArrayList<>();

  for (ResponseVTC r : rvtc){
    if (r!=null){
      JSONData data = new JSONData();
      data.setCapsules(r.capsules);
      data.setDistance(r.value);
      data.setTimestamp(r.timestamp);
      data.setFeeder(r.feeder);
      list.add(data);
    }
  }

  return list;
}
}
