package com.example.websocket.client.demo;

import com.example.websocket.client.demo.DOM.DataPointVTQ;
import com.example.websocket.client.demo.DOM.ResponseVTC;
import com.ge.predix.entity.timeseries.datapoints.ingestionrequest.Body;
import com.ge.predix.entity.timeseries.datapoints.ingestionrequest.DatapointsIngestion;
import com.ge.predix.solsvc.timeseries.bootstrap.client.TimeseriesClient;
import org.json.JSONException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by skarumanchi on 06/06/2017.
 */

public class TimeSeriesConsumer {

private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TimeSeriesConsumer.class);

@Autowired
private TimeseriesClient tsc;

private InMemoryQueue<ResponseVTC> imq;

@Value("${machineControllerId}")
private String machineId;


public TimeSeriesConsumer(InMemoryQueue<ResponseVTC> rvtc) {
  imq = rvtc;
  logger.info("New time series instance created");
}

@Scheduled(fixedDelay = 200)
public void checkQueue() {
  Iterator<ResponseVTC> i = imq.iterator();

  while (i.hasNext()) {
    ResponseVTC r = i.next();
    try {
      sendTimeSeriesData(r);
      i.remove();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}

public void sendTimeSeriesData(DataPointVTQ dataPointVTQ) throws JSONException {
  tsc.postDataToTimeseriesWebsocket(buildDpIngestion(dataPointVTQ));
}

public void sendTimeSeriesData(ResponseVTC responseVTC) throws JSONException {
  tsc.postDataToTimeseriesWebsocket(buildDpIngestion(responseVTC.feeder, machineId, responseVTC.timestamp, responseVTC.capsules));
}

private DatapointsIngestion buildDpIngestion(DataPointVTQ dvtq) {

  logger.info("New DatapointsIngestion request initiated");
  DatapointsIngestion di = new DatapointsIngestion();
  List<Body> bodies = new ArrayList<>();

  //Creates an empty body for the timeseries ingestion request
  Body bodyCapsules = new Body();
  //Sets the timeseries tag
  bodyCapsules.setName("CLERKENROASTERS:" + machineId + ":" + dvtq.getFeeder());
  //Creates an empty list that will be a single datapoint in the body of the timeseries.
  //The object will be: []
  List<Object> dp = new ArrayList<>();
  //Adds a timestamp into the first field in the list
  //[123123123123000]
  dp.add(dvtq.getTimestamp());
  //Adds the value that needs to be uploaded
  //[123123123123000, 1.0]
  dp.add(dvtq.getCapsules());

  //Creates an empty list that will be the body of the datapoints ingestion
  List<Object> dps = new ArrayList<>();

  //Adds the previously created timestamp, datapoint array into another array that will be the message body
  //[123123123123000, 1.0] -> [] = [[123123123123000, 1.0]]
  dps.add(dp);
  //Sets the datapoints
  bodyCapsules.setDatapoints(dps);
  //Adds the body as a body in the list of bodies in the ingestion request
  bodies.add(bodyCapsules);

  //Sets the body on the ingestion request
  di.setBody(bodies);

  logger.debug("Body generated: " + di.toString());
  return di;
}

private DatapointsIngestion buildDpIngestion(int feeder, String machineId, Long timestamp, Long capsules) {
  logger.info("New DatapointsIngestion request initiated");
  DatapointsIngestion di = new DatapointsIngestion();
  List<Body> bodies = new ArrayList<>();

  //Creates an empty body for the timeseries ingestion request
  Body bodyCapsules = new Body();
  //Sets the timeseries tag
  bodyCapsules.setName("CLERKENROASTERS:" + machineId + ":" + feeder);
  //Creates an empty list that will be a single datapoint in the body of the timeseries.
  //The object will be: []
  List<Object> dp = new ArrayList<>();
  //Adds a timestamp into the first field in the list
  //[123123123123000]
  dp.add(timestamp);
  //Adds the value that needs to be uploaded
  //[123123123123000, 1.0]
  dp.add(capsules);

  //Creates an empty list that will be the body of the datapoints ingestion
  List<Object> dps = new ArrayList<>();

  //Adds the previously created timestamp, datapoint array into another array that will be the message body
  //[123123123123000, 1.0] -> [] = [[123123123123000, 1.0]]
  dps.add(dp);
  //Sets the datapoints
  bodyCapsules.setDatapoints(dps);
  //Adds the body as a body in the list of bodies in the ingestion request
  bodies.add(bodyCapsules);

  //Sets the body on the ingestion request
  di.setBody(bodies);

  logger.debug("Body generated: " + di.toString());
  return di;
}


}
