package com.example.websocket.client.demo.DistanceGetter;

import com.example.websocket.client.demo.DOM.DataPointVTQ;
import com.example.websocket.client.demo.DOM.ResponseVTC;
import com.example.websocket.client.demo.DataGenerator;
import com.example.websocket.client.demo.InMemoryQueue;
import org.apache.commons.math3.stat.StatUtils;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

public class DistanceController implements DataGenerator {
private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DistanceController.class);
private static InMemoryQueue<DataPointVTQ> buffer;
private static InMemoryQueue<ResponseVTC> tsbuffer;
private static InMemoryQueue<ResponseVTC> wsbuffer;
NumberFormat formatter = new DecimalFormat("#0.00");
private DistanceThread distanceThread;
private Thread t;
private double setpoint[] = new double[8];
private double avg1[] = new double[8], avg2[] = new double[8];
private long capsules[] = new long[8];
private int samples = 60;
private boolean[] feeders = new boolean[8];
private ResponseVTC[] responses = new ResponseVTC[8];
private long latestDatapoint;

public DistanceController(InMemoryQueue<DataPointVTQ> imq, InMemoryQueue<ResponseVTC> rvtc, InMemoryQueue<ResponseVTC> wsbuffer) {
  tsbuffer = rvtc;
  buffer = imq;
  this.wsbuffer = wsbuffer;
  distanceThread = new DistanceThread(buffer, capsules);
  t = new Thread(distanceThread);
  t.start();
  logger.info("Successfully New DistanceController created.");
}

public long[] getCapsules() {
  long[] caps = capsules.clone();
  return caps;
}

public long[] setCapsules(long[] caps) {
  if (caps.length == 8) {
    for (int i = 0; i <= 7; i++) {
      capsules[i] = caps[i];
    }
    return capsules.clone();
  } else {
    logger.error("The array of capsules passed back is different from what we expected.");
    return null;
  }
}

public boolean[] getFeeders() {
  return feeders;
}

public ResponseVTC getDistance(int feeder) {
  return responses[feeder];
}

@Scheduled(fixedDelay = 500)
public void generateResponses() {
  long millis = System.currentTimeMillis();

  logger.info("Running scheduled generateResponses. There are " + buffer.size() + " values in the buffer");
  for (int i = 0; i <= 7; i++) {
    responses[i] = calculateDistance(i);
  }
  wsbuffer.addAll(Arrays.asList(responses));
  DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
  logger.info("Ran generateResponses in " + formatter.format(System.currentTimeMillis() - millis));
}


private ResponseVTC calculateDistance(int feeder) {
  int last = buffer.size() - 1;
  ResponseVTC responseVTC = new ResponseVTC();
  LinkedList<Double> myarray = new LinkedList<>();
  Double stDev;
  long timestamp = new Date().getTime();

  // Clean up the buffer
  for (int i = last; i >= 0; i--) {
    try {
      if (buffer.get(i) != null && buffer.get(i).getFeeder() == feeder) {
        if (myarray.size() >= samples) {
          buffer.remove(i);
        } else {
          if (buffer.get(i).getValue() != null) {
            myarray.add(buffer.get(i).getValue());
            timestamp = buffer.get(i).getTimestamp();
          }
        }
      }
    } catch (Exception e) {
      logger.error("There was an error getting the latest values: " + e.getLocalizedMessage());
      e.printStackTrace();
    }
  }

  if (myarray.size() > 2) {
    feeders[feeder] = true;
  } else {
    return null;
  }

  double[] array = new double[myarray.size()];
  int i = 0;
  for (Double s : myarray) {
    array[i] = s;
    i++;
  }

  avg1[feeder] = StatUtils.mean(array);
  stDev = Math.sqrt(StatUtils.populationVariance(array));

  //Once the standard deviation falls below x
  //and the setpoint has not been set
  //assume that there are 12 capsules and set the setpoint

  if (stDev <= 0.3) {
    // if there is no data in the setpoint, then estimate the number of capsules from scratch
    if (setpoint[feeder] == 0.0) {
      estimateCapsules(feeder);
    }

    //if there is a change of more than 0.6 cm, then update the capsule numbers
    if (Math.abs(avg2[feeder] - avg1[feeder]) >= 0.4) {
      if (avg1[feeder] > avg2[feeder]) {
        //if the new distance is LOWER than the old distance, decrement the number of capsules
        capsules[feeder] = capsules[feeder] - 1;
        avg2[feeder] = avg1[feeder];

        ResponseVTC total = new ResponseVTC();

        for (int r = 0; r < avg1.length; r++) {
          ResponseVTC rvtc = new ResponseVTC();
          rvtc.capsules = capsules[r];
          rvtc.value = avg1[r];
          rvtc.feeder = r;
          rvtc.timestamp = timestamp;

          total.timestamp = timestamp;
          total.capsules = total.capsules + capsules[r];
          total.value = total.value + 1/8.0 * avg1[r];
          total.feeder = 8;

          tsbuffer.add(rvtc);
        }
        tsbuffer.add(total);

      } else {
        //if the new distance is HIGHER than the old distance, re-estimate the number of capsules
        estimateCapsules(feeder);
      }
    }

  }

  responseVTC.capsules = capsules[feeder];
  responseVTC.value = avg1[feeder];
  responseVTC.feeder = feeder;
  responseVTC.timestamp = timestamp;

  logger.info("Feeder: " + feeder +
                  " Average: " + formatter.format(avg1[feeder]) +
                  " Average2: " + formatter.format(avg2[feeder]) +
                  " stDev: " + formatter.format(stDev) +
                  " Capsules: " + capsules[feeder] +
                  " based on " + myarray.size() + " values.");
  return responseVTC;
}

/**
 * This function estimates the number of capsules in the feeder
 **/
private void estimateCapsules(int feeder) {
  setpoint[feeder] = avg1[feeder];
  // if there is no data in the avg2 ,then set it to the current average
  avg2[feeder] = avg1[feeder];

  logger.info("Successfully set the setpoint to " + setpoint[feeder]);
  //estimating the number of capsules
  long set = Math.round((25 - setpoint[feeder]) / (1.1));

  if (set > 0) {
    capsules[feeder] = set;
  } else {
    capsules[feeder] = 0;
  }
  logger.info("Capsule number estimated at::" + capsules[feeder]);
}

public Integer getBufferSize() {
  return buffer.size();
}
@Override
public void setDataStore(InMemoryQueue<DataPointVTQ> queue) {
  buffer = queue;
}
}