package com.example.websocket.client.demo.DOM;

/**
 * Created by kpashov on 08/06/17.
 */
public interface DataPointVTQ {
public Double getValue();

public void setValue(Double value);

public Long getCapsules();

public void setCapsules(Long capsules);

public Long getTimestamp();

public void setTimestamp(Long timeStamp);

public int getFeeder();

public void setFeeder(int feeder);

public void setPersisted(boolean persisted);
public boolean isPersisted();


}
