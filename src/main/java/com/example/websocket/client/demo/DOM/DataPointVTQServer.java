package com.example.websocket.client.demo.DOM;

/**
 * Created by skarumanchi on 08/06/2017.
 */
public class DataPointVTQServer implements DataPointVTQ {
public Double value;
public Long capsules;
public Long timeStamp;
public int feeder;
public boolean persisted = false;
public DataPointVTQServer() {}
public DataPointVTQServer(Double value, Long capsules, Long timeStamp, int feeder) {
  this.value = value;
  this.capsules = capsules;
  this.timeStamp = timeStamp;
  this.feeder = feeder;
}

public Double getValue() {
  return value;
}
public void setValue(Double value) {
  this.value = value;
}
public Long getCapsules() {
  return capsules;
}
public void setCapsules(Long capsules) {
  this.capsules = capsules;
}
public Long getTimestamp() {
  return timeStamp;
}
public void setTimestamp(Long timeStamp) {
  this.timeStamp = timeStamp;
}
public int getFeeder() {
  return feeder;
}
public void setFeeder(int feeder) {
  this.feeder = feeder;
}

public boolean isPersisted() { return persisted;}
public void setPersisted(boolean persisted) { this.persisted = persisted; }

}
