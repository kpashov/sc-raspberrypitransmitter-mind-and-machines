package com.example.websocket.client.demo;

import com.ge.predix.solsvc.timeseries.bootstrap.client.TimeseriesClient;
import com.ge.predix.solsvc.timeseries.bootstrap.client.TimeseriesClientImpl;
import com.ge.predix.solsvc.timeseries.bootstrap.config.ITimeseriesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;

/**
 * Created by kpashov on 28/03/2017.
 */
public class WSTimeseriesWrapper extends TimeseriesClientImpl {

@Autowired
private TimeseriesClient timeseriesClient;
@Autowired
@Qualifier("defaultTimeseriesConfig")
private ITimeseriesConfig timeseriesConfig;

@PostConstruct
public void init() {
  try {
    this.timeseriesClient.createConnectionToTimeseriesWebsocket();
  } catch (Exception e) {
    throw new RuntimeException(
        "unable to set up timeseries Websocket Pool timeseriesConfig=" + this.timeseriesConfig, e);
  }
}

}