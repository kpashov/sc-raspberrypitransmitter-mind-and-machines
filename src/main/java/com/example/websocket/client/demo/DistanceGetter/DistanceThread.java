package com.example.websocket.client.demo.DistanceGetter;

import com.example.websocket.client.demo.DOM.DataPointVTQ;
import com.example.websocket.client.demo.DOM.DataPointVTQServer;
import com.example.websocket.client.demo.InMemoryQueue;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kpashov on 19/01/2017.
 */
public class DistanceThread implements Runnable {

public static final int ARDUINO_ADDR = 0x04;
private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DistanceThread.class);
I2CDevice arduino;
double averageNanosRead;
SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
private I2CBus i2c;
private InMemoryQueue<DataPointVTQ> buffer;
private long[] capsules = new long[8];
private String[] args = new String[]{"python", "./run-scripts/distance.py"};


public DistanceThread(InMemoryQueue<DataPointVTQ> list, long[] capsules) {
  buffer = list;
  this.capsules = capsules;
  byte results[] = new byte[33];
  try {
    i2c = I2CFactory.getInstance(I2CBus.BUS_1);
    arduino = i2c.getDevice(ARDUINO_ADDR);
    arduino.read(results, 0, 33);

    log.info("Response: " + results);
  } catch (Exception e) {
    log.error("Error opening a new i2c bus.");
    e.printStackTrace();
  }

}

@Override
public void run() {
  byte results[] = new byte[32];
  byte old_results[] = new byte[32];
  long timeStart = System.nanoTime();
  long estimaatedTime = 0;

  PrintWriter writer;
  try {
    writer = new PrintWriter("distancelog_" + new SimpleDateFormat("yyyyMMddhhmm").format(new Date()) + ".log", "UTF-8");
  } catch (Exception e) {
    e.printStackTrace();
    return;
  }

  while (true) {
    try {
      arduino.read(results, 0, 32);
      if (!Arrays.equals(results, old_results)) {
        List<DataPointVTQ> newVals = getDistancesFromByteArray(results);
        old_results = results.clone();

        for (DataPointVTQ vt : newVals) {
          buffer.add(vt);
          writer.println(vt.getTimestamp() + "," + vt.getValue() + "," + vt.getFeeder());
          writer.flush();
        }

        timeStart = System.nanoTime();
        estimaatedTime = 0;
      }
      Thread.sleep(50);
    } catch (Exception e) {
      log.error("Error getting data from Arduino.");
      e.printStackTrace();
    }
  }
}

private List<DataPointVTQ> getDistancesFromByteArray(byte[] response) {
  List<DataPointVTQ> myList = new LinkedList<>();

  for (int i = 0; i < 8; i++) {
    DataPointVTQ myVT;
    myVT = getDistanceFromByteArray(Arrays.copyOfRange(response, i * 4, (i + 1) * 4), i);
    if (myVT != null) {
      myList.add(myVT);
    }
  }
  return myList;
}

private DataPointVTQ getDistanceFromByteArray(byte[] byte_values, int feeder) {
  if (byte_values == null ||
      byte_values.length != 4) {
    log.error("Wrong argument passed to byte_values, returning null.");
    return null;
  }
  DataPointVTQ myDist = new DataPointVTQServer();
  byte arr[] = Arrays.copyOfRange(byte_values, 0, 4);
  double value = (double) ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN).getFloat();
  if (value != Double.NaN) {
    myDist.setValue(value);
    myDist.setFeeder(feeder);
    myDist.setTimestamp(new Date().getTime());
    return myDist;
  } else {
    return null;
  }

}
}