package com.example.websocket.client.demo.DOM;

public class ResponseVTC implements DataPointVTQ {
    public Double value = 0.0d;
    public Long timestamp = 0L;
    public int feeder;
    public Long capsules = 0L;
    public boolean persisted = false;

@Override
public Double getValue() {
    return value;
}
@Override
public void setValue(Double value) {
    this.value = value;
}
public Long getTimestamp() {
    return timestamp;
}
public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
}
@Override
public int getFeeder() {
    return feeder;
}
@Override
public void setFeeder(int feeder) {
    this.feeder = feeder;
}
@Override
public Long getCapsules() {
    return capsules;
}
@Override
public void setCapsules(Long capsules) {
    this.capsules = capsules;
}
@Override
public boolean isPersisted() {
    return persisted;
}
@Override
public void setPersisted(boolean persisted) {
    this.persisted = persisted;
}
}
