package com.example.websocket.client.demo.DOM;

public class JSONData
{
    private int feeder;
    private long timestamp;
    private double value;
    private double capsules;

    /**
     * @return -
     */
    public int getFeeder()
    {
        return this.feeder;
    }

    /**
     * @param feeder -
     */
    public void setFeeder(int feeder)
    {
        this.feeder = feeder;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getDistance() {
        return value;
    }

    public double getCapsules() {
        return capsules;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setDistance(double value) {
        this.value = value;
    }

    public void setCapsules(double capsules) {
        this.capsules = capsules;
    }
}